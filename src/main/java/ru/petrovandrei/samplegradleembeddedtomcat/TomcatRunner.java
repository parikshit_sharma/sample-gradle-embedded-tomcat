package ru.petrovandrei.samplegradleembeddedtomcat;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.catalina.Context;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import static java.lang.System.in;
import static java.lang.System.out;

public class TomcatRunner {

    private static final int
            PORT = 80;

    private static final String
            PATH        = "/test",
            LINK        = "http://127.0.0.1:" + PORT + PATH,
//            CLASSES_DIR = "target/classes"; //maven
            CLASSES_DIR = "build/classes"; //gradle

    static void onServerStarted() throws IOException {
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(
                    URI.create(LINK)
            );
        } else {
            out.println(LINK);
        }
    }

    public static void main(String[] a) throws Exception {
        Path dirRoot = Paths.get(".");
        
        String
                contextPath = "/",
                baseDirectory = resolve(dirRoot, "src/main/webapp/");

        Tomcat tomcat = new Tomcat();
        tomcat.setPort(PORT);

        Context context = tomcat.addWebapp(contextPath, baseDirectory);

        //<editor-fold defaultstate="collapsed" desc="Additions to make @WebServlet work">
        WebResourceRoot resources = new StandardRoot(context);
        resources.addPreResources(new DirResourceSet(
                resources, "/WEB-INF/classes", resolve(dirRoot, CLASSES_DIR), contextPath
        ));
        context.setResources(resources);
        //</editor-fold>

        tomcat.start();

        out.println("server started");
        onServerStarted();

        out.println("press enter to stop server");
        in.read();
    }

    private static String resolve(Path root, String sub) {
        return root.resolve(sub).toAbsolutePath().toString();
    }
}